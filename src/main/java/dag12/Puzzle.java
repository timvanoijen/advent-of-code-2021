package dag12;

import lombok.Getter;
import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 12;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();

        Map<String,List<String>> connections = new HashMap<>();
        while(lines.hasNext()) {
            String line = lines.next();
            String left = line.split("-")[0];
            String right = line.split("-")[1];
            connections.computeIfAbsent(left, k -> new ArrayList<>()).add(right);
            connections.computeIfAbsent(right, k -> new ArrayList<>()).add(left);
        }

        List<Path> closed = new ArrayList<>();
        List<Path> open = new LinkedList<>();
        Path startPath = new Path();
        open.add(startPath);

        while(!open.isEmpty()) {
            List<Path> newPaths = extend(open.remove(0), connections);
            newPaths.forEach(p -> ((p.getCur().equals("end")) ? closed : open).add(p));
        }

        System.out.println(closed.size());
    }

    private static List<Path> extend(Path path, Map<String, List<String>> connections){
        List<Path> result = new ArrayList<>();

        for(String conn : connections.get(path.getCur())) {
            Path extendedPath = path.clone();
            if(extendedPath.add(conn))
                result.add(extendedPath);
        }
        return result;
    }

    private static class Path {
        @Getter private String cur;
        @Getter private Set<String> prev;
        private boolean visitedSmallCaveTwice;

        public Path() {
            prev = new HashSet<>();
            cur = "start";
        }

        public boolean add(String newCur) {
            if (newCur.equals("start"))
                return false;

            if (newCur.equals(newCur.toLowerCase()) && prev.contains(newCur)) {
                if (visitedSmallCaveTwice)
                    return false;
                visitedSmallCaveTwice = true;
            }

            prev.add(cur);
            cur = newCur;
            return true;
        }

        public Path clone() {
            Path result = new Path();
            result.cur = cur;
            result.prev = new HashSet<>(prev);
            result.visitedSmallCaveTwice = visitedSmallCaveTwice;
            return result;
        }
    }
}
