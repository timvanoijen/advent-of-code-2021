package dag11;

import com.google.common.collect.Lists;
import lombok.Value;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle {

    private static final int dag = 11;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();

        // Parse
        Map<Point, Integer> levels = new HashMap<>();
        int rowIdx = 0;
        while(lines.hasNext()) {
            var rowHeights = Arrays.stream(lines.next().split(""))
                    .map(Integer::parseInt).collect(Collectors.toList());
            for(var columnIdx = 0; columnIdx < rowHeights.size(); columnIdx++)
                levels.put(new Point(columnIdx, rowIdx), rowHeights.get(columnIdx));
            rowIdx++;
        }

        int result1 = 0;
        int result2 = -1;
        int i = 0;
        while(result2 == -1) {
            levels.keySet().stream().forEach(p -> levels.put(p, (levels.get(p) + 1) % 10));
            Set<Point> flashed = levels.keySet().stream().filter(p -> levels.get(p) == 0).collect(Collectors.toSet());

            int nrFlashesInIteration = 0;
            while (!flashed.isEmpty()) {
                nrFlashesInIteration++;
                if (nrFlashesInIteration == 100)
                    result2 = i + 1;

                if (i < 100)
                    result1++;

                Point f = flashed.stream().findFirst().get();
                flashed.remove(f);
                flashed.addAll(extend(f, levels));
            }
            i++;
        }

        System.out.println(result1);
        System.out.println(result2);
    }

    private static Set<Point> extend(Point pt, Map<Point, Integer> levels){
        Set<Point> result = new HashSet<>();
        for(Point nb : pt.neighbourhood()) {
            if(!levels.containsKey(nb))
                continue;
            int l = levels.get(nb);
            if(l > 0) {
                if (l == 9)
                    result.add(nb);
                levels.put(nb, (l + 1) % 10);
            }
        }
        return result;
    }

    @Value
    private static class Point {
        int x;
        int y;

        public List<Point> neighbourhood() {
            return Lists.cartesianProduct(
                            IntStream.range(x - 1, x + 2).boxed().collect(Collectors.toList()),
                            IntStream.range(y - 1, y + 2).boxed().collect(Collectors.toList())
                    ).stream().filter(l -> l.get(0) != x || l.get(1) != y)
                    .map(l -> new Point(l.get(0), l.get(1))).collect(Collectors.toList());
        }
    }
}
