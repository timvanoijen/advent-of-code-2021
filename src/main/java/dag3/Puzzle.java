package dag3;

import utils.Utils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 3;

    public static void main(String[] args) {
        int nrBits = 12;
        List<String> lines = Utils.getInputLines(dag).collect(Collectors.toList());
        List<Integer> numbers = lines.stream().map(s -> Integer.parseInt(s,2)).collect(Collectors.toList());

        int gamma = calcValue(nrBits, numbers, (ones, zeros) -> ones >= zeros);
        int epsilon = calcValue(nrBits, numbers, (ones, zeros) -> (ones < zeros && ones > 0) || zeros == 0);

        System.out.printf(Long.toString(gamma * epsilon));
    }

    private static int calcValue(int nrBits, List<Integer> numbers, BiFunction<Integer, Integer, Boolean> sizeComparator) {
        for(int i = nrBits-1; i >= 0; i--) {
            int finalI = i;
            Map<Boolean, List<Integer>> groups = numbers.stream()
                    .collect(Collectors.groupingBy(x -> ((1 << finalI) & x) > 0));
            List<Integer> oneAtI = groups.getOrDefault(Boolean.TRUE, Collections.emptyList());
            List<Integer> zeroAtI = groups.getOrDefault(Boolean.FALSE, Collections.emptyList());
            numbers = sizeComparator.apply(oneAtI.size(), zeroAtI.size()) ? oneAtI : zeroAtI;
        }
        return numbers.get(0);
    }
}
