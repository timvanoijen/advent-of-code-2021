package dag15;

import lombok.Value;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 15;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        // Parse
        Map<Point, Integer> risksTemplate = new HashMap<>();
        int rowIdx = 0;
        while(lines.hasNext()) {
            var rowHeights = Arrays.stream(lines.next().split(""))
                    .map(Integer::parseInt).collect(Collectors.toList());
            for(var columnIdx = 0; columnIdx < rowHeights.size(); columnIdx++)
                risksTemplate.put(new Point(columnIdx, rowIdx), rowHeights.get(columnIdx));
            rowIdx++;
        }

        // Dimensions
        int xSize = risksTemplate.keySet().stream().mapToInt(Point::getX).max().getAsInt() + 1;
        int ySize = risksTemplate.keySet().stream().mapToInt(Point::getY).max().getAsInt() + 1;

        Map<Point, Integer> risks = new HashMap<>();
        for(var x = 0; x < 5; x++) {
            for(var y = 0; y < 5; y++) {
                for(Point p : risksTemplate.keySet()) {
                    risks.put(new Point(p.getX() + x*xSize, p.getY() + y*ySize),
                            (risksTemplate.get(p) + x + y - 1) % 9 + 1);
                }
            }
        }

        // Find shortest distance (using Dijkstra)
        Point topLeft = new Point(0,0);
        Point bottomRight = new Point( 5*xSize -1, 5*ySize - 1);
        Map<Point, Integer> shortest = new HashMap<>();
        shortest.put(bottomRight, 0);

        LinkedHashSet<Point> exploreSet = new LinkedHashSet<>();
        exploreSet.add(bottomRight);
        while(!exploreSet.isEmpty()) {
            Point toExplore = exploreSet.iterator().next();
            exploreSet.remove(toExplore);
            exploreSet.addAll(explore(toExplore, risks, shortest));
        }
        int result = shortest.get(topLeft);
        System.out.println(result);

        long finish = System.currentTimeMillis();
        System.out.println(String.format("Time elapsed: %d ms", finish - start));
    }

    // Explores point and returns new points that need to be explored
    private static Set<Point> explore(Point point, Map<Point, Integer> risks,
                                    Map<Point, Integer> shortest) {

        Set <Point> result = new HashSet<>();
        if (!risks.containsKey(point))
            return result;

        int shortestFromPoint = shortest.get(point);
        for(Point nb : point.neighbourhood()) {
            int altDistance = shortestFromPoint + risks.get(point);
            if (shortest.getOrDefault(nb, Integer.MAX_VALUE) > altDistance) {
                shortest.put(nb, altDistance);
                result.add(nb);
            }
        }
        return result;
    }

    @Value
    private static class Point {
        int x;
        int y;

        public List<Point> neighbourhood() {
            return Arrays.asList(
                    new Point(x + 1, y),
                    new Point(x - 1, y),
                    new Point(x, y + 1),
                    new Point(x, y - 1));
        }
    }
}
