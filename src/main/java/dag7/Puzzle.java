package dag7;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 7;

    public static void main(String[] args) {
        String input = Utils.getInputLines(dag).iterator().next();
        double[] numbers = Arrays.stream(input.split(",")).mapToDouble(Double::parseDouble).toArray();

        // Question 1
        int centerPoint = (int)new Median().evaluate(numbers);
        int fuel = (int)Arrays.stream(numbers).reduce(0, (f, p) -> f + Math.abs(p - centerPoint));
        System.out.println(Long.toString(fuel));

        // Question 2
        double centerPoint2Dbl = new Mean().evaluate(numbers);
        List<Integer> centerPoint2Options = Arrays.asList(
                (int)Math.floor(centerPoint2Dbl), (int)Math.ceil(centerPoint2Dbl));
        int fuel2 = centerPoint2Options.stream()
                .mapToInt(cp -> (int)Arrays.stream(numbers)
                        .reduce(0L, (f, p) -> f + Math.abs(p - cp)*(Math.abs(p - cp) + 1)/2))
                .min().getAsInt();
        System.out.println(Long.toString(fuel2));
    }
}
