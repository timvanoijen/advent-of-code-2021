package dag4;

import utils.Utils;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 4;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();
        List<Integer> drawnNumbers = Arrays.stream(lines.next().split(","))
                .map(Integer::parseInt).collect(Collectors.toList());

        Map<Integer,Integer> numberToIdx = new HashMap<>();
        for(int i = 0; i < drawnNumbers.size(); i++)
            numberToIdx.put(drawnNumbers.get(i),i);

        int highestWinningIdx = Integer.MIN_VALUE;
        long result = 0;
        while(lines.hasNext()) {
            lines.next();
            Integer[][] rows = new Integer[5][5];
            Integer[][] columns = new Integer[5][5];
            for(int y = 0; y < 5; y++) {
                String l = lines.next().trim();
                rows[y] = Arrays.stream(l.split("\\s+")).map(Integer::parseInt).toArray(Integer[]::new);
                for(int x = 0; x < 5; x++)
                    columns[x][y] = rows[y][x];
            }

            int minmaxIdxRows = Arrays.stream(rows).map(r -> Arrays.stream(r).map(numberToIdx::get).max(Integer::compareTo)).map(
                    Optional::get).min(Integer::compareTo).get();
            int minmaxIdxColumns = Arrays.stream(columns).map(r -> Arrays.stream(r).map(numberToIdx::get).max(Integer::compareTo)).map(
                    Optional::get).min(Integer::compareTo).get();
            int winningIdx = Integer.min(minmaxIdxColumns, minmaxIdxRows);

            if (winningIdx > highestWinningIdx) {
                highestWinningIdx = winningIdx;
                result = drawnNumbers.get(winningIdx) * Arrays.stream(rows).flatMap(Arrays::stream)
                        .filter(e -> numberToIdx.get(e) > winningIdx).mapToInt(Integer::intValue).sum();
            }
        }

        System.out.printf(Long.toString(result));
    }
}
