package dag10;

import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 10;

    private static final Map<String,String> chunks = new HashMap<>() {{
        put("(", ")");
        put("[", "]");
        put("{", "}");
        put("<", ">");
    }};

    private static final Map<String,Integer> errorScores = new HashMap<>() {{
        put(")", 3);
        put("]", 57);
        put("}", 1197);
        put(">", 25137);
    }};

    private static final Map<String, Integer> incompletionScores = new HashMap<>() {{
        put("(", 1);
        put("[", 2);
        put("{", 3);
        put("<", 4);
    }};

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();

        int totalErrorScore = 0;
        List<Long> allIncompletionScores = new ArrayList<>();
        while(lines.hasNext()) {
            String[] chars = lines.next().split("");
            Deque<String> stack = new ArrayDeque<>();
            boolean hasError = false;
            for(var i = 0; i < chars.length; i++) {
                String c = chars[i];
                if (chunks.containsKey(c)) {
                    stack.push(c);
                } else {
                    if (!c.equals(chunks.get(stack.pop()))) {
                        totalErrorScore += errorScores.get(c);
                        hasError = true;
                        break;
                    }
                }
            }

            if (!hasError) {
                allIncompletionScores.add(stack.stream()
                        .mapToLong(incompletionScores::get)
                        .reduce(0L, (a,b) -> 5*a + b));
            }
        };

        Long totalIncompletionScore = allIncompletionScores.stream()
                .sorted()
                .skip(allIncompletionScores.size() / 2).findFirst().get();
        System.out.println(totalErrorScore);
        System.out.println(totalIncompletionScore);
    }
}
