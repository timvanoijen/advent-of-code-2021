package dag19;

import dag19.linearalgebra.Matrix;
import dag19.linearalgebra.RotationsFactory;
import dag19.linearalgebra.Vec;
import org.apache.commons.lang3.StringUtils;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 19;

    private static final List<Matrix> rotations = RotationsFactory.createRotations();

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();
        List<List<Vec>> scanners = new ArrayList<>();

        // Parse
        while(lines.hasNext()) {
            lines.next(); // header
            List<Vec> recordings = new ArrayList<>();
            scanners.add(recordings);

            while(lines.hasNext()) {
                String line = lines.next();
                if (StringUtils.isEmpty(line))
                    break;
                String[] parts = line.split(",");
                recordings.add(new Vec(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2])));
            }
        }

        // Find scanner positions and beacons
        Queue<List<Vec>> scannersToExplore = scanners.stream().skip(1).collect(Collectors.toCollection(LinkedList::new));
        Set<Vec> known = new HashSet<>(scanners.get(0));
        Set<Vec> scannerPositions = new HashSet<>();

        while(!scannersToExplore.isEmpty()) {
            List<Vec> scanner = scannersToExplore.poll();
            Optional<Vec> scannerPosition = tryMatch(scanner, known);
            if (scannerPosition.isPresent())
                scannerPositions.add(scannerPosition.get());
            else
                scannersToExplore.add(scanner);
        }

        // Question 1
        System.out.println(known.size());

        // Question 2
        int maxDist = 0;
        for(Vec a : scannerPositions) {
            for(Vec b : scannerPositions) {
                maxDist = Math.max(maxDist, a.dist(b));
            }
        }
        System.out.println(maxDist);
    }

    private static Optional<Vec> tryMatch(List<Vec> scanner, Set<Vec> known) {

        for(Vec calibrateRecording : scanner) {
            for(Vec recordingKnown : known) {
                for(Matrix calibrateRotation : rotations) {
                    Vec calibrateTranslation = recordingKnown.minus(calibrateRotation.times(calibrateRecording));

                    int matches = 0;
                    for(Vec recording : scanner) {
                        if (known.contains(calibrateRotation.times(recording).plus(calibrateTranslation))) {
                            if (++matches == 12) {
                                known.addAll(scanner.stream().map(r -> calibrateRotation.times(r).plus(calibrateTranslation))
                                        .collect(Collectors.toList()));
                                return Optional.of(calibrateTranslation);
                            }
                        }
                    }
                }
            }
        }
        return Optional.empty();
    }
}