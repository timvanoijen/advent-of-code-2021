package dag19.linearalgebra;

import lombok.Value;

@Value
public class Vec {
    private final int x;
    private int y;
    private int z;

    public Vec(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vec plus(Vec b) {
        return new Vec(x + b.x, y + b.y, z + b.z);
    }

    public Vec minus(Vec b) {
        return plus(b.negate());
    }

    public Vec negate() {
        return new Vec(-x, -y, -z);
    }

    // cross-product
    public Vec cross(Vec b) {
        return new Vec(y*b.z - z*b.y, z*b.x - x*b.z, x*b.y - y*b.x);
    }

    public int dist(Vec b) {
        return Math.abs(x - b.x) + Math.abs(y - b.y) + Math.abs(z - b.z);
    }
}
