package dag19.linearalgebra;

import java.util.ArrayList;
import java.util.List;

public class RotationsFactory {

    public static List<Matrix> createRotations() {
        List result = new ArrayList();

        for(int xd = 0; xd < 3; xd ++) {
            for(int yd = 0; yd < 3; yd++) {
                if (xd == yd)
                    continue;
                for(int x_s = -1; x_s <= 1; x_s+=2) {
                    for(int y_s = -1; y_s <= 1; y_s+=2) {
                        Vec x_r = new Vec(xd == 0 ? x_s : 0, xd == 1 ? x_s : 0, xd == 2 ? x_s : 0);
                        Vec y_r = new Vec(yd == 0 ? y_s : 0, yd == 1 ? y_s : 0, yd == 2 ? y_s : 0);
                        Vec z_r = x_r.cross(y_r);
                        result.add(new Matrix(new Vec[] { x_r, y_r, z_r}));
                    }
                }
            }
        }
        return result;
    }

}
