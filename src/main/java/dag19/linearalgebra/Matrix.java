package dag19.linearalgebra;

import java.util.HashMap;
import java.util.Map;

public class Matrix {
    private final Vec[] cols;

    private final Map<Vec, Vec> timesCache = new HashMap<>();

    public Matrix(Vec[] cols) {
        this.cols = cols;
    }

    public Vec times(Vec v) {
        return timesCache.computeIfAbsent(v, a -> {
            int x = cols[0].getX() * a.getX() + cols[1].getX() * a.getY() + cols[2].getX() * a.getZ();
            int y = cols[0].getY() * a.getX() + cols[1].getY() * a.getY() + cols[2].getY() * a.getZ();
            int z = cols[0].getZ() * a.getX() + cols[1].getZ() * a.getY() + cols[2].getZ() * a.getZ();
            return new Vec(x, y, z);
        });
    }

}
