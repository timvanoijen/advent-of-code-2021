package dag5;

import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 5;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();

        Map<Integer, Integer> cnt = new HashMap<>();
        long result = 0;
        while(lines.hasNext()) {
            String l = lines.next();

            String[] coords = l.split(" -> ");
            int x1 = Integer.parseInt(coords[0].split(",")[0]);
            int y1 = Integer.parseInt(coords[0].split(",")[1]);
            int x2 = Integer.parseInt(coords[1].split(",")[0]);
            int y2 = Integer.parseInt(coords[1].split(",")[1]);

            int deltaX = x2 - x1;
            int deltaY = y2 - y1;

            //if (deltaX != 0 && deltaY != 0)
            //    continue;

            int maxDelta = Math.max(Math.abs(deltaX), Math.abs(deltaY));
            for (int i = 0; i <= maxDelta; i++) {
                int x_i = x1 + i * (deltaX / maxDelta);
                int y_i = y1 + i * (deltaY / maxDelta);
                int key = x_i * 10000 + y_i;

                int curCnt = cnt.getOrDefault(key, 0);
                if (curCnt == 1)
                    result++;
                cnt.put(key, curCnt+1);
            }
        }

        System.out.printf(Long.toString(result));
    }
}
