package dag13;

import com.google.common.collect.Sets;
import lombok.Value;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 13;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        //Parse coordinates
        Set<Point> points = new HashSet<>();
        while(true) {
            String line = lines.next();
            if (line.equals(""))
                break;
            int x = Integer.parseInt(line.split(",")[0]);
            int y = Integer.parseInt(line.split(",")[1]);
            points.add(new Point(x,y));
        }

        // Perform folding
        while(lines.hasNext()) {
            String line = lines.next();
            boolean isHor = line.split("=")[0].endsWith("x");
            int foldCoord = Integer.parseInt(line.split("=")[1]);

            points = isHor ? Sets.union(
                    points.stream().filter(p -> p.getX() < foldCoord).collect(Collectors.toSet()),
                    points.stream().filter(p -> p.getX() >= foldCoord)
                        .map(p -> new Point(2*foldCoord - p.getX(), p.getY())).collect(Collectors.toSet()))
                    : Sets.union(
                    points.stream().filter(p -> p.getY() < foldCoord).collect(Collectors.toSet()),
                    points.stream().filter(p -> p.getY() >= foldCoord)
                            .map(p -> new Point(p.getX(), 2*foldCoord - p.getY())).collect(Collectors.toSet()));
        }

        // Format output
        int width = points.stream().mapToInt(p -> p.getX()).max().getAsInt() + 1;
        int height = points.stream().mapToInt(p -> p.getY()).max().getAsInt() + 1;
        char[] spaces = new char[width];
        Arrays.fill(spaces, ' ');
        List<String> outputLines = new ArrayList<>(Collections.nCopies(height, new String(spaces)));
        for(Point p : points) {
            String line = outputLines.get(p.getY());
            outputLines.set(p.getY(), line.substring(0, p.getX()) + "*" + line.substring(p.getX() + 1));
        }

        for(String outputLine : outputLines)
            System.out.println(outputLine);
    }

    @Value
    private static class Point {
        private final int x;
        private final int y;
    }
}
