package dag18;

import lombok.Getter;
import lombok.Setter;
import utils.Utils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle {

    private static final int dag = 18;

    public static void main(String[] args) {
        List<String> lines = Utils.getInputLines(dag, false).collect(Collectors.toList());

        // Evaluate all combinations
        long maxMagnitude = 0;
        for(int i = 0; i < lines.size(); i++) {
            for(int j = 0; j < lines.size(); j++) {
                if (i == j)
                    continue;
                Node sum = addAndReduce(parse(lines.get(i)), parse(lines.get(j)));
                maxMagnitude = Math.max(maxMagnitude, sum.getMagnitude());
            }
        }

        System.out.println(maxMagnitude);
    }

    private static Node parse(String line) {
        Node curNode = new Node();
        for(String s : line.split("")) {
            if (s.equals("[")) {
                curNode = curNode.addChild(new Node());
            } else if (s.equals("]")) {
                curNode = curNode.getParent();
            } else if (s.equals(",")) {
                curNode = curNode.getParent().addChild(new Node());
            } else {
                curNode.setValue(curNode.getValue() * 10 + Integer.parseInt(s));
            }
        }
        return curNode;
    }

    private static Node addAndReduce(Node node1, Node node2) {

        // Read input
        Node root = new Node() {{
            addChild(node1);
            addChild(node2);
        }};

        // Keep exploding and splitting until no explosions or splits are left
        while(true) {
            List<Node> nodes = root.depthFirstCollect();
            List<Node> leafNodes = nodes.stream().filter(Node::isLeaf).collect(Collectors.toList());

            // First, try to explode a node
            Optional<Node> nodeToExplode = nodes.stream().filter(n -> n.isLeafContainer() && n.getLevel() > 3).findFirst();
            if (nodeToExplode.isPresent()) {
                Node n = nodeToExplode.get();
                Map<Node, Integer> leafNodeIdx = IntStream.range(0, leafNodes.size()).boxed()
                        .collect(Collectors.toMap(leafNodes::get, Function.identity()));

                int firstLeftIdx = leafNodeIdx.get(n.getLeft()) - 1;
                if (firstLeftIdx >= 0) {
                    Node firstLeftNode = leafNodes.get(firstLeftIdx);
                    firstLeftNode.setValue(firstLeftNode.getValue() + n.getLeft().getValue());
                }

                int firstRightIdx = leafNodeIdx.get(n.getRight()) + 1;
                if (firstRightIdx < leafNodes.size()) {
                    Node firstRightNode = leafNodes.get(firstRightIdx);
                    firstRightNode.setValue(firstRightNode.getValue() + n.getRight().getValue());
                }
                n.children.clear();
                n.setValue(0);
                continue;
            }

            // Then, try to split
            Optional<Node> nodeToSplit = leafNodes.stream().filter(n -> n.getValue() > 9).findFirst();
            if (nodeToSplit.isPresent()) {
                Node n = nodeToSplit.get();
                n.addChild(new Node()).setValue(n.getValue() / 2);
                n.addChild(new Node()).setValue((n.getValue() + 1) / 2);
                continue;
            }
            break;
        }
        return root;
    }

    private static class Node {

        @Getter @Setter private int value;
        @Getter private int level;
        @Getter private Node parent;
        private List<Node> children = new ArrayList<>();

        public Node addChild(Node child) {
            child.parent = this;
            for (Node n : child.depthFirstCollect())
                n.level += (this.level + 1);
            this.children.add(child);
            return child;
        }

        public boolean isLeaf() {
            return children.isEmpty();
        }

        public boolean isLeafContainer() {
            return children.size() == 2 && getLeft().isLeaf() && getRight().isLeaf();
        }

        public Node getLeft() {
            return children.get(0);
        }

        public Node getRight() {
            return children.get(1);
        }

        public long getMagnitude() {
            if (isLeaf())
                return value;
            else
                return 3*getLeft().getMagnitude() + 2*getRight().getMagnitude();
        }

        public List<Node> depthFirstCollect() {
            List<Node> result = new ArrayList<>();
            result.add(this);
            result.addAll(children.stream().flatMap(c ->
                    c.depthFirstCollect().stream()).collect(Collectors.toList()));
            return result;
        }

        @Override
        public String toString() {
            if (isLeaf())
                return Integer.toString(value);
            return String.format("[%s,%s]", getLeft(), getRight());
        }
    }
}