package dag25;

import lombok.Value;
import utils.Utils;

import java.util.*;


public class Puzzle {

    private static final int dag = 25;

    private static int width = 0;
    private static int height = 0;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        List<Point> east = new ArrayList<>();
        List<Point> south = new ArrayList<>();
        Map<Point, Integer> eastIdx = new HashMap<>();
        Map<Point, Integer> southIdx = new HashMap<>();

        // Parse
        int y = 0;
        int x = 0;
        while(lines.hasNext()) {
            String line = lines.next();
            x = 0;
            for(String p : line.split("")) {
                Point pt = new Point(x, y);
                if (p.equals(">")) {
                    eastIdx.put(pt, east.size());
                    east.add(pt);
                } else if (p.equals("v")) {
                    southIdx.put(pt, south.size());
                    south.add(pt);
                }
                x++;
            }
            y++;
        }
        height = y;
        width = x;

        // Steps
        int cnt = 0;
        Set<Point> potentialEastMovers = new HashSet<>(east);
        Set<Point> potentialSouthMovers = new HashSet<>(south);

        while(potentialSouthMovers.size() > 0 || potentialEastMovers.size() > 0) {

            // East
            List<Point> moves = new ArrayList<>();
            for(Point p : potentialEastMovers) {
                Integer idx = eastIdx.get(p);
                if (idx == null)
                    continue;;
                Point p2 = p.right();
                if (eastIdx.get(p2) == null && southIdx.get(p2) == null)
                    moves.add(p);
            }

            potentialEastMovers = new HashSet<>();
            for(Point p : moves) {
                Integer idx = eastIdx.get(p);
                Point p2 = p.right();
                east.set(idx, p2);
                eastIdx.put(p, null);
                eastIdx.put(p2, idx);
                potentialEastMovers.add(p2);
                potentialEastMovers.add(p.left());
                potentialSouthMovers.add(p.up());
                potentialSouthMovers.remove(p2.up());
            }

            // South
            moves = new ArrayList<>();
            for(Point p : potentialSouthMovers) {
                Integer idx = southIdx.get(p);
                if (idx == null)
                    continue;;
                Point p2 = p.down();
                if (eastIdx.get(p2) == null && southIdx.get(p2) == null)
                    moves.add(p);
            }

            potentialSouthMovers = new HashSet<>();
            for(Point p : moves) {
                Integer idx = southIdx.get(p);
                Point p2 = p.down();
                south.set(idx, p2);
                southIdx.put(p, null);
                southIdx.put(p2, idx);
                potentialSouthMovers.add(p2);
                potentialSouthMovers.add(p.up());
                potentialEastMovers.add(p.left());
                potentialEastMovers.remove(p2.left());
            }
            cnt++;
        }

        System.out.println(cnt);
    }

    @Value
    private static class Point {
        private final int x;
        private final int y;

        public Point left() {
            return new Point((x + width - 1) % width, y);
        }

        public Point right() {
            return new Point((x + 1) % width, y);
        }

        public Point up() {
            return new Point(x, (y + height - 1) % height);
        }

        public Point down() {
            return new Point(x, (y + 1) % height);
        }
    }
}