package dag17;

public class Puzzle {

    private static final int dag = 17;

    public static void main(String[] args) {

        // Input
        int xmin = 277; int xmax = 318;
        int ymin = -92; int ymax = -53;

        int result = 0;
        for(int vy = ymin; vy <= -ymin; vy++) {
            for(int vx = 1; vx <= xmax; vx++) {
                for(int t = 1; t <= -2*ymin + 1; t++) {
                    int x = (Math.min(t, vx) * (vx + 1 + Math.max(vx - t, 0))) / 2;
                    int y = (t * (vy + (vy + 1 - t))) / 2;
                    if (xmin <= x && x <= xmax && ymin <= y && y <= ymax) {
                        result++;
                        break;
                    }
                }
            }
        }

        System.out.println(result);
    }
}
