package dag6;

import utils.Utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 6;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();
        Map<Integer,Long> groups = Arrays.stream(lines.next().split(","))
                .map(Integer::parseInt)
                .collect(Collectors.groupingBy(Function.identity()))
                .entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> Long.valueOf(e.getValue().size())));

        for(int i = 0; i < 256; i++) {
            Map<Integer, Long> groupsNew = new HashMap<>();
            for(int j = 0; j < 8; j++)
                groupsNew.put(j, groups.getOrDefault(j + 1, 0L));
            groupsNew.put(8, groups.getOrDefault(0, 0L));
            groupsNew.put(6, groupsNew.get(6) + groups.getOrDefault(0, 0L));
            groups = groupsNew;
        }

        long result = groups.values().stream().reduce(0L, (a,b) -> a + b);

        System.out.printf(Long.toString(result));
    }
}
