package dag23;

import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 23;
    private static final int heightRoom = 4;
    private static final int numberOfPlayers = 16;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        // Build initial state
        int[][] positionOfPlayer = new int[numberOfPlayers][2];
        int[][] playerAtPosition = new int[13][heightRoom + 3];
        int[] playerTypeCount = new int[4];
        for (int y = 0; y < heightRoom + 3; y++) {
            String line = lines.next();
            for (int x = 0; x < 13; x++) {
                char c = x < line.length() ? line.charAt(x) : '#';
                int playerType = "ABCD".indexOf(c);
                if (playerType == -1) {
                    playerAtPosition[x][y] = -1;
                    continue;
                }
                int playerNumber = (playerType * heightRoom + playerTypeCount[playerType]++);
                positionOfPlayer[playerNumber][0] = x;
                positionOfPlayer[playerNumber][1] = y;
                playerAtPosition[x][y] = playerNumber;
            }
        }

        GameState state = new GameState(positionOfPlayer, playerAtPosition);

        int result = getMin(state);

        System.out.println(result);
    }

    private static int getMin(GameState state) {
        PriorityQueue<Pair<GameState, Integer>> queue =
                new PriorityQueue<>(Comparator.comparing(Pair::getRight));

        Set<GameState> evaluated = new HashSet<>();
        queue.add(Pair.of(state, 0));

        while(!queue.isEmpty()) {
            Pair<GameState, Integer> p = queue.poll();
            GameState s = p.getLeft();
            int cumEnergy = p.getRight();

            if (evaluated.contains(s))
                continue;
            evaluated.add(s);
            
            if (s.solved())
                return cumEnergy;

            Move m = s.getFirstAllowedRoomMove();
            if (m != null) {
                queue.add(Pair.of(s.applyMove(m), cumEnergy + m.getEnergy()));
            } else {
                for(Move m2 : s.getAllowedHallwayMoves()) {
                    queue.add(Pair.of(s.applyMove(m2), cumEnergy + m2.getEnergy()));
                }
            }
        }
        return -1;
    }

    @Value
    private static class Move {
        private static final int[] energyLevels = { 1, 10, 100, 1000 };

        private final int player;
        private final int[] from;
        private final int[] till;

        public int getEnergy() {
            int steps = Math.abs(1 - from[1]) + Math.abs(1 - till[1]) + Math.abs(from[0] - till[0]);
            return steps * energyLevels[player / heightRoom];
        }
    }

    @Value
    private static class GameState {

        private final static int[] hallwayCols = { 1, 2, 4, 6, 8, 10, 11 };
        private final static int[] roomCols = { 3, 5, 7, 9 };
        private final static int hallwayRow = 1;

        private final int[][] positionOfPlayer;
        private final int[][] playerAtPosition;

        // Just lazy:
        // Ugly (not very efficient) trick to overcome the burden of implementing equals and hashcode for 2D array.
        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if(!(o instanceof GameState))
                return false;
            return toString().equals(o.toString());
        }

        public boolean solved() {
            for(int i = 0; i < numberOfPlayers; i++) {
                if (!isInRoom(i, roomOfPlayer(i)))
                    return false;
            }
            return true;
        }

        public GameState applyMove(Move move) {
            int[][] positionOfPlayer = deepCopy(this.positionOfPlayer);
            int[][] playerAtPosition = deepCopy(this.playerAtPosition);
            playerAtPosition[move.getFrom()[0]][move.getFrom()[1]] = -1;
            playerAtPosition[move.getTill()[0]][move.getTill()[1]] = move.getPlayer();
            positionOfPlayer[move.getPlayer()] = move.getTill();
            return new GameState(positionOfPlayer, playerAtPosition);
        }

        public Move getFirstAllowedRoomMove() {
            for(int i = 0; i < numberOfPlayers; i++) {
                Move move = getMoveForPlayerToRoomDestination(i);
                if (move != null)
                    return move;
            }
            return null;
        }

        public List<Move> getAllowedHallwayMoves() {
            List<Move> result = new ArrayList<>();
            for(int player = 0; player < numberOfPlayers; player++) {
                if(!isInRoom(player))
                    continue;
                for(int s = 0; s < hallwayCols.length; s++) {
                    int[] destination = hallwaySpotToPosition(s);
                    if (isPathFree(positionOfPlayer[player], destination))
                        result.add(new Move(player, positionOfPlayer[player], destination));
                }
            }
            return result;
        }

        private Move getMoveForPlayerToRoomDestination(int player) {

            int destinationRoom = roomOfPlayer(player);

            // If player already in room, it cannot move there
            if (isInRoom(player, destinationRoom))
                return null;

            int posInRoom = heightRoom;
            while(--posInRoom >= 0) {
                int[] destinationPosition = roomSpotToPosition(destinationRoom, posInRoom);
                int playerAtDestination = playerAtPosition[destinationPosition[0]][destinationPosition[1]];
                if (playerAtDestination == -1) {
                    if (!isPathFree(positionOfPlayer[player], destinationPosition))
                        return null;
                    return new Move(player, positionOfPlayer[player], destinationPosition);
                } else if (roomOfPlayer(playerAtDestination) != roomOfPlayer(player)) {
                    break;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for(int y = 0; y < playerAtPosition[0].length; y++) {
                for(int x = 0; x < playerAtPosition.length; x++) {
                    char c = '.';
                    if (playerAtPosition[x][y] > -1) {
                        int playerType = roomOfPlayer(playerAtPosition[x][y]);
                        c = "ABCD".charAt(playerType);
                    }
                    sb.append(c);
                }
                sb.append(System.lineSeparator());
            }
            return sb.toString();
        }

        // Helper methods

        private boolean isPathFree(int[] posA, int[] posB) {
            int x = posA[0];
            int y = posA[1];
            if (playerAtPosition[posB[0]][posB[1]] > -1)
                return false;

            while (y > 1)
                if (playerAtPosition[x][--y] > -1)
                    return false;
            while (x != posB[0]) {
                x += ((x < posB[0]) ? 1 : -1);
                if (playerAtPosition[x][y] > -1)
                    return false;
            }
            while (y != posB[1]) {
                if (playerAtPosition[x][++y] > -1)
                    return false;
            }
            return true;
        }

        private int[] roomSpotToPosition(int room, int pos) {
            return new int[] { roomCols[room], hallwayRow + 1 + pos };
        }

        private int[] hallwaySpotToPosition(int hallwaySpot) {
            return new int[] { hallwayCols[hallwaySpot], hallwayRow };
        }

        private boolean isInRoom(int player) {
            return positionOfPlayer[player][1] > hallwayRow;
        }

        private boolean isInRoom(int player, int room) {
            return isInRoom(player) && roomCols[room] == positionOfPlayer[player][0];
        }

        private int roomOfPlayer(int player) {
            return player / heightRoom;
        }

        private int[][] deepCopy(int[][] in) {
            int[][] result = new int[in.length][in[0].length];
            for (int i = 0; i < in.length; i++)
                result[i] = Arrays.copyOf(in[i], in[i].length);
            return result;
        }
    }
}
