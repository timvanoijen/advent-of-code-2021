package dag24;

import com.google.common.collect.Lists;
import lombok.Value;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import utils.Utils;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 24;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();
        Program program = Program.fromInput(lines);

        List<Long> variables = new ArrayList<>(Collections.nCopies(4, 0L));
        List<Integer> firstNumberList = program.findFirstMonadNumber(variables, 0, new HashSet<>());
        String result = Lists.reverse(firstNumberList).stream().map(Object::toString).collect(Collectors.joining(""));
        System.out.println(result);
    }

    private static class Program {

        private List<Operation> operations = new ArrayList<>();

        public static Program fromInput(Iterator<String> lines) {
            List<Operation> operations = new ArrayList<>();

            while(lines.hasNext()) {
                String line = lines.next();
                String[] parts = line.split(" ");
                String op = parts[0];
                int vIdx = getVariable(parts[1]);
                ValueHolder v2 = parts.length == 3 ? getValueHolder(parts[2]): null;

                if (op.equals("inp")) {
                    operations.add(new Operation(true, vIdx, (input, vars) -> Long.valueOf(input)));
                } else if (op.equals("add")) {
                    operations.add(new Operation(false, vIdx, ($, vars) -> vars.get(vIdx) + v2.getValue(vars)));
                } else if (op.equals("mul")) {
                    operations.add(new Operation(false, vIdx, ($, vars) -> vars.get(vIdx) * v2.getValue(vars)));
                } else if (op.equals("div")) {
                    operations.add(new Operation(false, vIdx, ($, vars) -> vars.get(vIdx) / v2.getValue(vars)));
                } else if (op.equals("mod")) {
                    operations.add(new Operation(false, vIdx, ($, vars) -> vars.get(vIdx) % v2.getValue(vars)));
                } else if (op.equals("eql")) {
                    operations.add(new Operation(false, vIdx, ($, vars) -> ((vars.get(vIdx) == v2.getValue(vars)) ? 1L : 0)));
                } else
                    throw new RuntimeException();
            }

            Program result = new Program();
            result.operations = operations;
            return result;
        }

        // Performs depth-first search for a valid Monad number, cutting-off branches whose state at a certain point
        // is equal to a state that has failed before.
        public List<Integer> findFirstMonadNumber(List<Long> variables, int startInstruction,
                                               Set<Pair<Long, Integer>> failedStates) {

            // If this state has failed before, immediately return null. At this point, just before a new input
            // we know that only the z-variable matters, since w gets his new value at the next input and x and y
            // are always reset to zero before they are used in the next cycle. Therefore, it suffices to use a key
            // that exists only of the z-variable and the startInstruction. Using this simplified key, makes
            // that we can recognize "clashing" states one iteration level earlier, which has a double positive effect
            // on the performance. 1) We need less iterations 2) our hashset will be smaller, resulting in less hashcode
            // collisions.
            var stateKey = Pair.of(variables.get(3), startInstruction);
            if (failedStates.contains(stateKey))
                return null;

            // We reached the end. In case the z-variable equals 0, we return an empty array, indicating we found the
            // solution.
            Operation operation = getOperation(startInstruction);
            if (operation == null) {
                return variables.get(3) == 0 ? new ArrayList<>() : null;
            }

            // Iterate over the possible input values
            for(int i = 1; i < 10; i++) {
                List<Long> variables_i = new ArrayList<>(variables);
                // 1) Provide the value to the input step
                operation.apply(i, variables_i);
                // 2) Execute the program till the next input step or the end
                int nextInstruction = executeTillNextInputOrEnd(variables_i, startInstruction + 1);
                // 3) Recursively find the first Monad number on the inner problem
                List<Integer> result = findFirstMonadNumber(variables_i,  nextInstruction, failedStates);
                if (result != null) {
                    result.add(i);
                    return result;
                }
            }

            // We did not find a Monad number, so we add the entry state to the set of failed states.
            failedStates.add(stateKey);
            return null;
        }

        private int executeTillNextInputOrEnd(List<Long> variables, int startInstruction) {
            while(true) {
                Operation operation = getOperation(startInstruction);
                if(operation == null || operation.needsInput)
                    return startInstruction;
                operation.apply(-1, variables);
                startInstruction++;
            }
        }

        public Operation getOperation(int nr) {
            if (nr < operations.size())
                return operations.get(nr);
            return null;
        }

        private static int getVariable(String s) {
            return "wxyz".indexOf(s);
        }

        private static ValueHolder getValueHolder(String s) {
            if (NumberUtils.isCreatable(s))
                return $ -> Long.parseLong(s);
            int vIdx = getVariable(s);
            return (variables) -> variables.get(vIdx);
        }

        @FunctionalInterface
        private interface ValueHolder {
            long getValue(List<Long> variables);
        }

        @Value
        public static class Operation {
            private final boolean needsInput;
            private final int resultVariable;
            private final BiFunction<Integer, List<Long>, Long> action;

            public void apply(int input, List<Long> variables) {
                variables.set(resultVariable, action.apply(input, variables));
            }
        }
    }
}
