package dag22;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 22;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        Map<Body, Integer> bodyCounts = new HashMap<>();
        while (lines.hasNext()) {

            // Parse body
            String line = lines.next();
            String[] p1 = line.split(" ");
            boolean on = p1[0].equals("on");
            Body body = Body.fromString(p1[1]);

            // Compensate for overlap with existing bodies by introducing compensating bodies
            for(Map.Entry<Body, Integer> bodyCount : Maps.newHashMap(bodyCounts).entrySet()) {
                Optional<Body> union = body.union(bodyCount.getKey());
                if (union.isEmpty())
                    continue;
                int unionValue = bodyCounts.getOrDefault(union.get(), 0) -bodyCount.getValue();
                bodyCounts.put(union.get(), unionValue);
            }

            // Add itself in case it is turned on
            if (on) {
                bodyCounts.put(body, bodyCounts.getOrDefault(body, 0) + 1);
            }
        }

        // Calculate result
        long result = bodyCounts.entrySet().stream().mapToLong(e -> e.getKey().getVolume() * e.getValue()).sum();
        System.out.println(result);
    }

    private static class Body extends ArrayList<Pair<Integer, Integer>> {

        public static Body fromString(String l) {
            String[] p = l.split(",");
            Body body = new Body();
            for(int i = 0; i < p.length; i++) {
                String[] bounds = p[i].split("=")[1].split("\\.\\.");
                body.add(Pair.of(Integer.parseInt(bounds[0]), Integer.parseInt(bounds[1])));
            }
            return body;
        }

        public long getVolume() {
            return this.stream().mapToLong(p -> p.getRight() - p.getLeft() + 1).reduce(1L, (a, b) -> a * b);
        }

        public Optional<Body> union(Body r) {
            Body union = new Body();
            for(int i = 0; i < this.size(); i++) {
                if (this.get(i).getLeft() > r.get(i).getRight() || this.get(i).getRight() < r.get(i).getLeft())
                    return Optional.empty();
                union.add(Pair.of(Math.max(this.get(i).getLeft(), r.get(i).getLeft()), Math.min(this.get(i).getRight(), r.get(i).getRight())));
            }
            return Optional.of(union);
        }
    }
}
