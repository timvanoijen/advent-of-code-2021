package dag20;

import lombok.Value;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 20;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        // Parse replacement values
        List<Boolean> values = Arrays.stream(lines.next().split(""))
                .map(s -> s.equals("#")).collect(Collectors.toList());

        // Parse initial image
        lines.next();
        int y = 0;
        Map<Point, Boolean> image = new HashMap<>();
        while(lines.hasNext()) {
            String line = lines.next();
            for(int x = 0; x < line.length(); x++)
                image.put(new Point(x, y), line.charAt(x) == '#');
            y++;
        }

        // Apply enhancements
        boolean def = false;
        for(int i = 0; i < 50; i++) {
            Map<Point, Boolean> newImage = new HashMap<>();
            int ymin = image.keySet().stream().mapToInt(Point::getY).min().getAsInt() - 1;
            int ymax = image.keySet().stream().mapToInt(Point::getY).max().getAsInt() + 1;
            int xmin = image.keySet().stream().mapToInt(Point::getX).min().getAsInt() - 1;
            int xmax = image.keySet().stream().mapToInt(Point::getX).max().getAsInt() + 1;

            for(int x = xmin; x <= xmax; x++) {
                for(int y2 = ymin; y2 <= ymax; y2++) {
                    Point p = new Point(x, y2);
                    newImage.put(p, getEnhancedValue(p, image, values, def));
                }
            }
            image = newImage;
            def = values.get(def ? 511 : 0);
        }

        // Question 1
        long nrLitPoints = image.values().stream().filter(v -> v).count();
        System.out.println(nrLitPoints);

    }

    private static boolean getEnhancedValue(Point p, Map<Point, Boolean> image, List<Boolean> values, boolean def) {
        return values.get(p.neighbourhood().stream()
                .map(p2 -> image.getOrDefault(p2, def) ? 1 : 0)
                .reduce(0, (a,b) -> (a << 1) + b));
    }

    @Value
    private static class Point {
        int x;
        int y;

        public List<Point> neighbourhood() {
            List<Point> result = new ArrayList<>();
            for(int yi = y - 1; yi <= y +1; yi++) {
                for(int xi = x - 1; xi <= x +1; xi++) {
                    result.add(new Point(xi, yi));
                }
            }
            return result;
        }
    }
}
