package dag2;

import utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 2;

    public static void main(String[] args) {
        List<String> lines = Utils.getInputLines(dag).collect(Collectors.toList());
        int x = 0;
        int depth = 0;
        int aim = 0;
        for(String l : lines) {
            String[] parts = l.split(" ");
            String move = parts[0];
            int amount = Integer.parseInt(parts[1]);

            switch(move) {
                case "forward":
                    x += amount;
                    depth += aim * amount;
                    break;
                case "up":
                    aim -= amount;
                    break;
                case "down":
                    aim += amount;
                    break;
            }

        }

        System.out.printf(Long.toString(x * depth));
    }
}
