package dag1;

import utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class Puzzle2 {

    private static final int dag = 1;

    public static void main(String[] args) {
        int cnt = 0;
        List<Integer> values = Utils.getInputLines(dag).map(Integer::parseInt).collect(Collectors.toList());

        for(int i = 0; i + 3 < values.size(); i++)
            cnt += values.get(i+3) > values.get(i) ? 1 : 0;

        System.out.printf(Long.toString(cnt));
    }
}
