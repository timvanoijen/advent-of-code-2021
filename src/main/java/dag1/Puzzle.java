package dag1;

import utils.Utils;

import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 1;

    public static void main(String[] args) {
        int prev = Integer.MAX_VALUE;
        int cnt = 0;

        for(String s : Utils.getInputLines(dag).collect(Collectors.toList())) {
            int cur = Integer.parseInt(s);
            cnt += cur > prev ? 1 : 0;
            prev = cur;
        };

        System.out.printf(Long.toString(cnt));
    }
}
