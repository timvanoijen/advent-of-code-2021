package dag21;

import lombok.Value;

import java.util.*;

public class Puzzle {

    private static final int dag = 21;

    private static final Map<Integer, Integer> nrCombinationsForSum = new HashMap<>();

    public static void main(String[] args) {

        int[] p = new int[] { 2, 10}; // real input
        //int[] p = new int[] { 4, 8}; // test input
        int[] q = new int[] { 0, 0};

        for(int i1 =1; i1 < 4; i1++)
            for(int i2 =1; i2 < 4; i2++)
                for(int i3 =1; i3 < 4; i3++)
                    nrCombinationsForSum.put(i1 + i2 + i3, nrCombinationsForSum.getOrDefault(i1+i2+i3, 0) + 1);

        long result = nrWins(new State(0, p, q), new HashMap<>());
        System.out.println(result);
    }

    // Find number of wins for player 0, given initial state, using DP.
    private static long nrWins(State s, Map<State, Long> cache) {
        if (cache.containsKey(s))
            return cache.get(s);

        // If game finished, return 1 in case player 1 won.
        if (s.getQ()[0] >= 21)
            return 1;
        else if (s.getQ()[1] >= 21)
            return 0;

        // Calculate number of wins by evaluating next step options.
        long result = 0;
        for(int dp = 3; dp < 10; dp++) {
            int[] p2 = s.getP().clone();
            int[] q2 = s.getQ().clone();
            p2[s.turn] = (p2[s.turn] + dp - 1) % 10 + 1;
            q2[s.turn] += p2[s.turn];
            State s2 = new State((s.turn + 1) % 2, p2, q2);
            result += nrCombinationsForSum.get(dp) * nrWins(s2, cache);
        }

        cache.put(s, result);
        return result;
    }

    @Value
    private static class State {
        private final int turn;
        private final int[] p;
        private final int[] q;
    }
}
