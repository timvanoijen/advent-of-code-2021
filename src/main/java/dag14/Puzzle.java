package dag14;

import org.apache.commons.lang3.StringUtils;
import utils.Utils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 14;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        // Parse
        String sequence = lines.next();
        lines.next();
        Map<String, List<String>> transformations = new HashMap<>();
        while (lines.hasNext()) {
            String line = lines.next();
            String pattern = line.split(" -> ")[0];
            String subs = line.split(" -> ")[1];
            transformations.put(pattern, Arrays.asList(pattern.substring(0,1) + subs, subs + pattern.substring(1)));
        }

        // Apply replacements
        Map<String, Long> counts = transformations.keySet().stream().collect(
                Collectors.toMap(Function.identity(), k -> (long)StringUtils.countMatches(sequence, k)));

        for (var i = 0; i < 40; i++) {
            Map<String, Long> newCounts = new HashMap<>();
            for(String tFrom : transformations.keySet()) {
                for(String tTo : transformations.get(tFrom)) {
                    Long newCount = newCounts.getOrDefault(tTo, 0L) +
                            counts.getOrDefault(tFrom, 0L);
                    newCounts.put(tTo, newCount);
                }
            }
            counts = newCounts;
        }

        // Format output
        Map<String, Long> charCounts = counts.entrySet().stream().collect(Collectors.groupingBy(
                e -> e.getKey().substring(1), Collectors.summingLong(e -> e.getValue())));
        // * First character of sequence not count yet.
        charCounts.put(sequence.substring(0,1), 1 + charCounts.getOrDefault(sequence.substring(0,1), 0L));
        var sortedCharCounts = charCounts.values().stream().sorted().collect(Collectors.toList());
        long result = sortedCharCounts.get(sortedCharCounts.size() -1) - sortedCharCounts.get(0);
        System.out.println(result);
    }
}
