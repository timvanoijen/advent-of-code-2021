package dag16.packets;

import dag16.BinaryStream;

public final class LiteralPacket implements Packet {
    private final int version;
    private long literalNumber = 0;

    public LiteralPacket(int version) {
        this.version = version;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void initialize(BinaryStream s) {
        literalNumber = 0;
        boolean cont = true;
        while(cont) {
            cont = s.next();
            literalNumber = (literalNumber << 4) + s.nextInteger(4);
        }
    }

    @Override
    public long calculateValue() {
        return literalNumber;
    }
}
