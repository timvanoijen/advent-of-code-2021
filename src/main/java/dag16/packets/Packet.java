package dag16.packets;

import dag16.BinaryStream;

public interface Packet {
    int getVersion();
    void initialize(BinaryStream s);
    long calculateValue();
}
