package dag16.packets;

import dag16.BinaryStream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

public class OperatorPacket implements Packet {

    private final int version;
    private final Operator operator;
    private final List<Packet> children;

    public OperatorPacket(int version, Operator operator) {
        this.version = version;
        this.operator = operator;
        this.children = new ArrayList<>();
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void initialize(BinaryStream s) {
        if (s.next()) {
            // Type 1 sub packet definition
            int nrSubpackets = s.nextInteger(11);
            while(nrSubpackets-- > 0)
                children.add(new PacketFactory().createPackage(s));
        } else {
            // Type 0 sub packet definition
            BinaryStream subStream = s.select(s.nextInteger(15));
            while(subStream.hasNext())
                children.add(new PacketFactory().createPackage(subStream));
        }
    }

    @Override
    public long calculateValue() {
        return operator.calculateValue(children.stream()
                .mapToLong(Packet::calculateValue));
    }

    @FunctionalInterface
    public interface Operator {
        long calculateValue(LongStream values);
    }
}
