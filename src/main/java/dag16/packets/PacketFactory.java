package dag16.packets;

import dag16.BinaryStream;

import java.util.List;
import java.util.stream.Collectors;

public class PacketFactory {

    public Packet createPackage(BinaryStream s) {
        int version = s.nextInteger(3);
        int typeId = s.nextInteger(3);

        Packet result;
        if (typeId == 4) {
            result = new LiteralPacket(version);
        } else if (typeId == 0) {
            result = new OperatorPacket(version, ls -> ls.sum());
        } else if (typeId == 1) {
            result = new OperatorPacket(version, ls -> ls.reduce(1L, (a,b) -> a * b));
        } else if (typeId == 2) {
            result = new OperatorPacket(version, ls -> ls.min().getAsLong());
        } else if (typeId == 3) {
            result = new OperatorPacket(version, ls -> ls.max().getAsLong());
        } else if (typeId == 5) {
            result = new OperatorPacket(version, ls -> {
                List<Long> l = ls.boxed().collect(Collectors.toList());
                return l.get(0) > l.get(1) ? 1 : 0;
            });
        } else if (typeId == 6) {
            result = new OperatorPacket(version, ls -> {
                List<Long> l = ls.boxed().collect(Collectors.toList());
                return l.get(0) < l.get(1) ? 1 : 0;
            });
        } else if (typeId == 7) {
            result = new OperatorPacket(version, ls -> {
                List<Long> l = ls.boxed().collect(Collectors.toList());
                return l.get(0).equals(l.get(1)) ? 1 : 0;
            });
        } else {
            throw new RuntimeException(String.format("Packet type %d ", typeId));
        }

        result.initialize(s);
        return result;
    }
}
