package dag16;

import dag16.packets.PacketFactory;
import utils.Utils;

import java.util.*;

public class Puzzle {

    private static final int dag = 16;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();
        BinaryStream s = BinaryStream.fromHexString(lines.next());

        long result = new PacketFactory().createPackage(s).calculateValue();
        System.out.println(result);
    }
}
