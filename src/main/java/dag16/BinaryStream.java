package dag16;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class BinaryStream {

    private final Iterator<Boolean> iterator;

    public static BinaryStream fromHexString(String input) {
        List<Boolean> bits = new ArrayList<>();
        for(String s : input.split("")) {
            byte i = Byte.parseByte(s, 16);
            for(int j = 3; j >= 0; j--)
                bits.add((i & 1 << j) > 0);
        }
        return new BinaryStream(bits.iterator());
    }

    private BinaryStream(Iterator<Boolean> iterator) {
        this.iterator = iterator;
    }

    public BinaryStream select(int nrBits) {
        List<Boolean> copied = new ArrayList<>();
        while(nrBits-- > 0)
            copied.add(next());
        return new BinaryStream(copied.iterator());
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public boolean next() {
        return iterator.next();
    }

    public int nextInteger(int nrBits) {
        int result = 0;
        while(nrBits-- > 0)
            result = (result << 1) + (next() ? 1 : 0);
        return result;
    }
}
