package dag9;

import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

    private static final int dag = 9;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();

        // Parse
        List<List<Integer>> heights = new ArrayList<>();
        while(lines.hasNext()) {
            List<Integer> row = Arrays.stream(lines.next().split(""))
                    .map(Integer::parseInt).collect(Collectors.toList());
            heights.add(row);
        }

        int xSize = heights.get(0).size();
        int ySize = heights.size();

        // Convolution
        int result1 = 0;
        for(var x = 0; x < xSize; x++) {
            for(var y = 0; y < ySize; y++) {
                boolean isLowest = true;
                int height = heights.get(y).get(x);
                for(var xx = Math.max(x-1,0); xx < Math.min(x+2, xSize); xx++) {
                    for (var yy = Math.max(y-1,0); yy < Math.min(y+2, ySize); yy++) {
                        if ((xx == x && yy == y) || (xx != x && yy != y))
                            continue;
                        isLowest = isLowest && heights.get(yy).get(xx) > height;
                    }
                }
                result1 += isLowest ? height + 1 : 0;
            }
        }
        System.out.println(result1);
    }
}
