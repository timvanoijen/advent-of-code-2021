package dag9;

import com.google.common.collect.Lists;
import lombok.Value;
import utils.Utils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle2 {

    private static final int dag = 9;

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag, false).iterator();

        // Parse
        Map<Point, Integer> heights = new HashMap<>();
        int rowIdx = 0;
        while(lines.hasNext()) {
            var rowHeights = Arrays.stream(lines.next().split(""))
                    .map(Integer::parseInt).collect(Collectors.toList());
            for(var columnIdx = 0; columnIdx < rowHeights.size(); columnIdx++)
                heights.put(new Point(columnIdx, rowIdx), rowHeights.get(columnIdx));
            rowIdx++;
        }

        // Find basins
        Map<Point, Point> basinsCache = new HashMap<>();
        heights.keySet().stream().filter(p -> heights.get(p) != 9)
                .forEach(p -> getBasin(p, heights, basinsCache));

        // Calculate result
        int result = basinsCache.values().stream().collect(Collectors.groupingBy(Function.identity()))
                .values().stream().mapToInt(b -> -b.size())
                .sorted().limit(3)
                .reduce(1, (a,b) -> a * -b);
        System.out.println(result);
    }

    // Recursively find basin for point. Results are memoized.
    private static Point getBasin(Point point, Map<Point, Integer> heights, Map<Point, Point> cache) {
        if (cache.containsKey(point))
            return cache.get(point);

        Point lowestNeighbour = point.neighbourhood().stream().min(
                Comparator.comparing(p -> heights.getOrDefault(p, Integer.MAX_VALUE))).get();
        Point basin = (lowestNeighbour.equals(point)) ? point : getBasin(lowestNeighbour, heights, cache);
        cache.put(point, basin);
        return basin;
    }

    @Value
    private static class Point {
        int x;
        int y;

        public List<Point> neighbourhood() {
            return Lists.cartesianProduct(
                    IntStream.range(x - 1, x + 2).boxed().collect(Collectors.toList()),
                    IntStream.range(y - 1, y + 2).boxed().collect(Collectors.toList())
            ).stream().filter(l -> l.get(0) == x || l.get(1) == y)
                    .map(l -> new Point(l.get(0), l.get(1))).collect(Collectors.toList());
        }
    }
}
