package dag8;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import utils.Utils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle {

    private static final int dag = 8;
    private static String alphabet = "abcdefg";
    private static final Map<List<Integer>, Integer> correctSignals = new HashMap() {{
            put(signalToIntegerList("abcefg"), 0);
            put(signalToIntegerList("cf"), 1);
            put(signalToIntegerList("acdeg"), 2);
            put(signalToIntegerList("acdfg"), 3);
            put(signalToIntegerList("bcdf"), 4);
            put(signalToIntegerList("abdfg"), 5);
            put(signalToIntegerList("abdefg"), 6);
            put(signalToIntegerList("acf"), 7);
            put(signalToIntegerList("abcdefg"), 8);
            put(signalToIntegerList("abcdfg"), 9);
        }};

    public static void main(String[] args) {
        Iterator<String> lines = Utils.getInputLines(dag).iterator();

        List<List<Integer>> permutations = permute(IntStream.range(0, 7)
                .boxed().collect(Collectors.toList()));

        int result1 = 0;
        int result2 = 0;

        while(lines.hasNext()) {
            String line = lines.next();
            String[] left = line.split("\\|")[0].trim().split(" ");
            String[] right = line.split("\\|")[1].trim().split(" ");

            List<List<Integer>> trainingNumbers = Arrays.stream(left)
                    .map(Puzzle::signalToIntegerList).collect(Collectors.toList());
            List<List<Integer>> outputNumbers = Arrays.stream(right)
                    .map(Puzzle::signalToIntegerList).collect(Collectors.toList());

            List<Integer> secretPermutation = permutations.stream()
                    .filter(p -> trainingNumbers.stream().allMatch(n -> getCorrectedNumber(n, p).isPresent()))
                    .findFirst().get();

            for(var i = 0; i < 4; i++) {
                int correctedNumber = getCorrectedNumber(outputNumbers.get(i), secretPermutation).get();
                result1 += Arrays.asList(1, 4, 7, 8).contains(correctedNumber) ? 1 : 0;
                result2 += correctedNumber * Math.pow(10, 3 - i);
            }

        }
        System.out.println(result1);
        System.out.println(result2);
    }

    private static List<Integer> signalToIntegerList(String string) {
        return IntStream.range(0, string.length())
                .map(c -> alphabet.indexOf(string.charAt(c)))
                .boxed().collect(Collectors.toList());
    }

    private static Optional<Integer> getCorrectedNumber(List<Integer> input, List<Integer> permutation) {
        List<Integer> correctedInput = input.stream().map(permutation::get)
                .sorted().collect(Collectors.toList());
        return Optional.ofNullable(correctSignals.get(correctedInput));
    }

    private static List<List<Integer>> permute(List<Integer> l) {
        if(l.size() == 0)
            return Arrays.asList(l);

        return l.stream().flatMap(i ->
                    permute(l.stream().filter(e -> e != i).collect(Collectors.toList())).stream()
                        .map(p -> Lists.newArrayList(Iterables.concat(p, Arrays.asList(i))))
                ).collect(Collectors.toList());
    }
}
